from django.shortcuts import render
from django.http import HttpResponse
from .models import ToDoItem
# 'from' keyword allows importing of necessary classes, methods and other items needed in our application. 
#  While 'import' keyword defines what we are importing from the package

# Create your views here.

def index(request):
	todoitem_list = ToDoItem.objects.all()
	# return HttpResponse("Hello from the views.py file")  # Session 1

	# output = ', '.join([todoitem.task_name for todoitem in todoitem_list])
	# return HttpResponse(output)

	# template = loader.get_template("todolist/index.html")

	context = {
		'todoitem_list': todoitem_list
	}
	# return HttpResponse(template.render(context, request))
	return render(request, "todolist/index.html", context)


def todoitem(request, todoitem_id):
	response = "You are viewing the details of %s"
	return HttpResponse(response % todoitem_id)